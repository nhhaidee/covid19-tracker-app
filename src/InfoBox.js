import React from "react";
import { Card, CardContent, Typography } from "@material-ui/core";
import "./InfoBox.css";

function InfoBox(props) {

  return (
    <Card onClick={props.onClick} className={`infoBox ${props.active && "infoBox--selected"} ${props.isRed && "infoBox--red"}
          ${props.isBlue && "infoBox--blue"} ${props.isYellow && "infoBox--yellow"}`}>
      <CardContent>
        <Typography color="textSecondary" gutterBottom>
          {props.title}
        </Typography>
        <h2 className={`${props.isRed && "infoBox__cases__red"} ${props.isBlue && "infoBox__cases__blue"} ${props.isYellow && "infoBox__cases__yellow"}`}>
          Today: {props.cases}
        </h2>
        <Typography className="infoBox__total" color="textSecondary">
          Total: {props.total} 
        </Typography>
      </CardContent>
    </Card>
  );
}

export default InfoBox;
