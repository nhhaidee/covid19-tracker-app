import React from "react";
import "./Table.css";
import numeral from "numeral";

function Table({ countries }) {
  return (
    <div className="table">
      <tr>
        <th>Country</th>
        <th>Total Cases</th> 
        <th>Deaths</th>
        <th>Recovered</th>
        <th>Active Cases</th>
      </tr>
      {countries.map((country) => (
        <tr>
          <td>{country.country}</td>
          <td>
            <strong>{numeral(country.cases).format("0,0")}</strong>
          </td>
          <td>
            <strong>{numeral(country.deaths).format("0,0")}</strong>
          </td>
          <td>
            <strong>{numeral(country.recovered).format("0,0")}</strong>
          </td>
          <td>
            <strong>{numeral(country.active).format("0,0")}</strong>
          </td>
        </tr>
      ))}
    </div>
  );
}

export default Table;
