import React, { useState, useEffect } from "react";
import Header from './components/Header';
import "./App.css";
import {
  MenuItem,
  Select,
  Card,
  CardContent,
} from "@material-ui/core";
import InfoBox from "./InfoBox";
import LineGraph from "./LineGraph";
import Table from "./Table";
import { sortData, prettyPrintStat } from "./util";
import numeral from "numeral";
import Map from "./Map";
import "leaflet/dist/leaflet.css";

const App = () => {

  const [country, setInputCountry] = useState("worldwide"); //select a country name
  const [countryName, getCountryName] = useState("");
  const [countryInfo, setCountryInfo] = useState({}); // all country information
  const [countries, setCountries] = useState([]); // array of countries
  const [mapCountries, setMapCountries] = useState([]);
  const [tableData, setTableData] = useState([]);
  const [casesType, setCasesType] = useState("cases");
  const [mapCenter, setMapCenter] = useState({ lat: 34.80746, lng: -40.4796 });
  const [mapZoom, setMapZoom] = useState(3);
  
  useEffect(() => {
    getCountryName("Worldwide");
    fetch("https://disease.sh/v3/covid-19/all") // Statistic for world wide in total
      .then((response) => response.json())
      .then((data) => {setCountryInfo(data)});
  }, []);
  
  useEffect(() => {

    const getCountriesData = async () => {
      await fetch("https://disease.sh/v3/covid-19/countries") // Statistics for each particular country
        .then((response) => response.json())
        .then((data) => {
          const countries = data.map((country) => ({
            country_name: country.country, // Get country name such as United State, United Kingdom
            country_code: country.countryInfo.iso2, //Get country code such as UK, USA
          }));
          let sortedData = sortData(data);
          setCountries(countries);
          setMapCountries(data);
          setTableData(sortedData);
        });
    };

    getCountriesData();
  }, []);
  const onCountryChange = async (event) => {

    const countryCode = event.target.value;
    let url ="";

    if (countryCode === "worldwide"){
      url= "https://disease.sh/v3/covid-19/all";     
      getCountryName("Worldwide");
    }
    else{
      url = `https://disease.sh/v3/covid-19/countries/${countryCode}`;
    } 
    await fetch(url)
    .then((response) => response.json())
    .then((data) => {
      setInputCountry(countryCode);
      setCountryInfo(data);
      if (countryCode !== "worldwide"){
        getCountryName(data.country);
        setMapCenter([data.countryInfo.lat, data.countryInfo.long]);
      } 
      setMapZoom(4);
    });
  };
  return (
    
    <div>
      <Header></Header>
      <br></br> 
      <div className="row row-content">
        <div className="col-12">
          <h5>Select Country</h5>
        </div>
        <div className="col-md-2">
            <div className="form-group">
              <Select  className="form-control" variant="outlined" value={country} onChange={onCountryChange}>
                <MenuItem value="worldwide">Worldwide</MenuItem>
                 {countries.map((countryItems) => (<MenuItem value={countryItems.country_code}>{countryItems.country_name}</MenuItem>))}
              </Select>
            </div>
        </div>
      </div>
      <div className="row row-content">
        <div className="col-12 col-md m-1">
          <InfoBox
            onClick={(e) => setCasesType("cases")}
            title="Coronavirus Cases"
            isYellow
            active={casesType === "cases"}
            cases={prettyPrintStat(countryInfo.todayCases)}
            total={numeral(countryInfo.cases).format("0.0a")}
          />
        </div>
        <div className="col-12 col-md m-1">
          <InfoBox
            onClick={(e) => setCasesType("recovered")}
            title="Recovered"
            isBlue
            active={casesType === "recovered"}
            cases={prettyPrintStat(countryInfo.todayRecovered)}
            total={numeral(countryInfo.recovered).format("0.0a")}
          />
        </div>
        <div className="col-12 col-md m-1">
          <InfoBox
            onClick={(e) => setCasesType("deaths")}
            title="Deaths"
            isRed
            active={casesType === "deaths"}
            cases={prettyPrintStat(countryInfo.todayDeaths)}
            total={numeral(countryInfo.deaths).format("0.0a")}
          />
        </div>
      </div>
      <br></br>
      <div className="row row-content">
        <div className="col-12 col-md-6 offset-md-3">
          <Card>
            <CardContent>
              <h3>{countryName} {casesType} Daily Change</h3>
              <LineGraph casesType={casesType} countryCode={country}/>
            </CardContent>
          </Card>
        </div>


      </div>
      <br></br>
      <div className="row row-content">
        <div className="col-12 col-md-8">
          <h3>By Map</h3>
          <Map
              countries={mapCountries}
              casesType={casesType}
              center={mapCenter}
              zoom={mapZoom}
            />
        </div>
        <div className="col-12 col-md-4">
          <h3>Live Statistics by Country</h3>
          <Table countries={tableData} />

        </div>
      </div>
    </div>
  );
};

export default App;
