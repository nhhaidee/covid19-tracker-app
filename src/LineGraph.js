import React, { useState, useEffect } from "react";
import { Line } from "react-chartjs-2";
import numeral from "numeral";
import "./App.css";


const options = {
  legend: {
    display: false,
  },
  elements: {
    point: {
      radius: 0,
    },
  },
  title:{
    display:true,
    fontColor: "black",
    fontFamily: "Gamja Flower",
    fontSize: 25,
    fontStyle: "bold"
  },
  maintainAspectRatio: false,
  tooltips: {
    mode: "index",
    intersect: false,
    callbacks: {
      label: function (tooltipItem, data) {
        return numeral(tooltipItem.value).format("+0,0");
      },
    },
  },
  scales: {
    xAxes: [
      {
        type: "time",
        gridLines: {
          display: false,
        },
        scaleLabel: {
          display: true,
          fontSize: 15,
          fontStyle: "bold"
        },
        time: {
          format: "MM/DD/YY",
          tooltipFormat: "ll",
        },
      },
    ],
    yAxes: [
      {
        gridLines: {
          display: true,
        },
        scaleLabel: {
          display: true,
          fontSize: 15,
          fontStyle: "bold"
        },
        ticks: {
          // Include a dollar sign in the ticks
          callback: function (value, index, values) {
            return numeral(value).format("0a");
          },
        },
      },
    ],
  },
};

const buildChartData = (data, casesType) => {
  let chartData = [];
  let lastDataPoint = 0;
  for (let date in data.cases) {
    if (lastDataPoint){
      let newDataPoint = {
        x: date,
        y: data[casesType][date] - lastDataPoint,
      };
      chartData.push(newDataPoint);
    }
    lastDataPoint = data[casesType][date];
  }
  return chartData;
};

function LineGraph(props) {

  const [data, setData] = useState({});

  useEffect(() => {
    const url = props.countryCode === "worldwide"? "https://disease.sh/v3/covid-19/historical/all?lastdays=90": `https://disease.sh/v3/covid-19/historical/${props.countryCode}?lastdays=90`;
    
    if (props.countryCode === "worldwide"){
      console.log("HHHHHHHHHHHHHHHHHHHH")
      const fetchData = async () => {
        await fetch(url)
          .then((response) => {
            return response.json();
          })
          .then((data) => {
              let chartData = buildChartData(data, props.casesType);
              setData(chartData);
          });
      };
      fetchData();
    }

    else{
      const fetchData = async () => {
        await fetch(url)
          .then((response) => {
            return response.json();
          })
          .then((data) => {
            let chartData = buildChartData(data.timeline, props.casesType);
            setData(chartData);
          });
      };
      fetchData();
    }

  }, [props.casesType, props.countryCode]); 

  let borderChartColor ="";
  if (props.casesType === "cases"){
    borderChartColor = "rgb(255, 187, 0)";
  }
  else if (props.casesType === "recovered"){
    borderChartColor = "blue";
  } 
  else{
    borderChartColor = "red";
  }
  return (
    <div>
      {data && data.length > 0 && (
        <Line
          data={{
            datasets: [
              {
                borderColor: borderChartColor,
                label:"Population",
                data: data,
                fill:false,
              },
            ],
          }}
          options={options} height={500}
        />
    )}
    </div>
  );
}

export default LineGraph;
